import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector } from '@angular/core';
import { AuthBarComponent } from './auth-bar.component';
import { createCustomElement } from '@angular/elements';

@NgModule({
  declarations: [
    AuthBarComponent
  ],
  imports: [
    BrowserModule
  ],
  entryComponents: [
    AuthBarComponent
  ]
})
export class AppModule {
  constructor(private injector: Injector) {
    const AuthBar = createCustomElement(AuthBarComponent, { injector });
    customElements.define('auth-bar', AuthBar);
  }

  ngDoBootstrap(app) {}
}
