import { Component, ViewEncapsulation } from '@angular/core';
import { AuthBloc } from 'logic/blocs/auth.bloc';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'auth-bar',
  templateUrl: './auth-bar.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [AuthBloc]
})
export class AuthBarComponent {
  constructor(public authBloc: AuthBloc) {

  }

}
