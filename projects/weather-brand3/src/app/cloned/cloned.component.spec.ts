import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClonedComponent } from './cloned.component';

describe('ClonedComponent', () => {
  let component: ClonedComponent;
  let fixture: ComponentFixture<ClonedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClonedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClonedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
