import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HomeComponent } from 'projects/weather-brand1/src/app/home/home.component';
import { AuthBarComponent } from 'projects/auth-bar/src/app/auth-bar.component';
import { WeatherComponent } from 'projects/weather-brand1/src/app/weather/weather.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from 'projects/weather-brand1/src/app/app-routing.module';
import { SharedModule } from 'projects/shared/shared.module';
import { ClonedComponent } from './cloned/cloned.component';
import { SecondLevelModule } from './second-level/second-level.module';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AuthBarComponent,
    WeatherComponent,
    ClonedComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    SharedModule,
    SecondLevelModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
