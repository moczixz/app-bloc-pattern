export const environment = {
  production: true,
  weatherLocations: ['Berlin', 'London', 'Warsaw', 'New York']
};
