import { Component } from '@angular/core';
import { HomeBloc } from 'logic/blocs/home.bloc';
import { Observable } from 'rxjs';
import { FormArray, FormControl, FormGroup } from '@angular/forms';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [HomeBloc]
})
export class HomeComponent {

  public locationsForm$: Observable<FormGroup>;

  constructor(public homeBloc: HomeBloc) {
    this.locationsForm$ = homeBloc.weatherLocations$
      .pipe(
        map(locations =>
          new FormGroup({
            locations: new FormArray(
              locations.map(location => new FormControl(location))
            )
          })
        )
      );
  }
}
