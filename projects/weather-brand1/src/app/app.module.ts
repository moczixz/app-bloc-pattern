import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { WeatherComponent } from './weather/weather.component';
import { SharedModule } from 'projects/shared/shared.module';
import { AuthBarComponent } from 'projects/auth-bar/src/app/auth-bar.component';
import { ClonedComponent } from './cloned/cloned.component';
import { SecondLevelModule } from './second-level/second-level.module';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    WeatherComponent,
    AuthBarComponent,
    ClonedComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    SharedModule,
    SecondLevelModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
