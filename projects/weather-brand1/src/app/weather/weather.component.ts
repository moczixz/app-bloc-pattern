import { Component } from '@angular/core';
import { WeatherBloc } from 'logic/blocs/weather.bloc';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css'],
  providers: [WeatherBloc]
})
export class WeatherComponent {

  constructor(public weatherBloc: WeatherBloc) {
  }

}
