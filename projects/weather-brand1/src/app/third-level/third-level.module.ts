import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThirdLevelComponent } from './third-level.component';

@NgModule({
  declarations: [ThirdLevelComponent],
  imports: [
    CommonModule
  ],
  exports: [
    ThirdLevelComponent
  ]
})
export class ThirdLevelModule { }
