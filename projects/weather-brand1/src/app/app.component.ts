import { Component } from '@angular/core';
import { ConfigService } from 'logic/shared/config.service';
import { environment } from '../environments/environment.prod';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'weather-brand1';
  constructor(private config: ConfigService) {
    config.environment = environment;
  }
}
