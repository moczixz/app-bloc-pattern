import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThirdLevelModule } from '../third-level/third-level.module';
import { SecondLevelComponent } from './second-level.component';

@NgModule({
  declarations: [SecondLevelComponent],
  imports: [
    CommonModule,
    ThirdLevelModule
  ],
  exports: [
    SecondLevelComponent
  ]
})
export class SecondLevelModule { }
