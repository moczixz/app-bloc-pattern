import { Environment } from 'logic/shared/interfaces/environment.interface';

export const environment: Environment = {
  production: true,
  weatherLocations: ['Berlin', 'London', 'Warsaw', 'New York']
};
