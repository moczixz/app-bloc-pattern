import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'nothing'})
export class NothingPipe implements PipeTransform {
  transform(value: any): number {
    return value;
  }
}
