import { NgModule } from '@angular/core';
import { NothingPipe } from './pipes/nothing.pipe';

@NgModule({
  declarations: [NothingPipe],
  exports: [NothingPipe]
})
export class SharedModule {

}
