import { Injectable } from '@angular/core';
import { Environment } from './interfaces/environment.interface';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  environment: Environment;

}
