export interface WeatherConfig {
  refreshInterval: number;
  shuffleInterval: number;
  shuffleNumber: number;
  locations: string[];
}
