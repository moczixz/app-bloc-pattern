export interface Environment {
  production: boolean;
  weatherLocations: string[];
}
