export interface WeatherData {
  id: number;
  name: string;
  main: {
    temp: number;
    pressure: number;
    humidity: number;
    temp_min: number;
    temp_max: number;
  };
  wind: {
    speed: number;
    deg: number;
  };
  rain: any;
  snow: any;
  clouds: {
    all: number;
  };
  weather: Array<{
      id: number;
      main: string;
      description: string;
      icon: string
    }>;
}
