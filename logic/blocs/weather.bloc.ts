import { Injectable, OnDestroy } from '@angular/core';
import { WeatherService } from 'logic/services/weather.service';
import { Subject, Observable, BehaviorSubject } from 'rxjs';
import { WeatherData } from 'logic/shared/interfaces/weather-data.interface';
import { takeUntil, startWith, switchMap, map } from 'rxjs/operators';
import { ConfigService } from 'logic/shared/config.service';

@Injectable()
export class WeatherBloc implements OnDestroy {

  private weatherDataOutput$: BehaviorSubject<WeatherData[]> = new BehaviorSubject<WeatherData[]>([]);

  private onDestroy$: Subject<void> = new Subject<void>();

  constructor(private weatherService: WeatherService, private config: ConfigService) {
    this.init();
  }

  public get weatherData$(): Observable<WeatherData[]> {
    return this.weatherDataOutput$.asObservable();
  }

  private init(): void {
    this.getWeather();
  }

  private getWeather(): void {
    this.weatherService.getChangedLocations()
      .pipe(
        startWith(this.config.environment.weatherLocations),
        map(locations => ({
          refreshInterval: 10000,
          shuffleInterval: 60000,
          shuffleNumber: 3,
          locations,
        })),
        switchMap(config => this.weatherService.getWeatherForConfig(config)),
        takeUntil(this.onDestroy$)
      )
    .subscribe(weatherData => this.weatherDataOutput$.next(weatherData));
  }

  public ngOnDestroy(): void {
    this.onDestroy$.next(null);
    this.onDestroy$.complete();
  }

}
