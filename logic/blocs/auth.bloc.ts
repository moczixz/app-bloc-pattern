import { Injectable, OnDestroy } from '@angular/core';
import { AuthService } from 'logic/services/auth.service';
import { Subject, Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Injectable()
export class AuthBloc implements OnDestroy {

  public signInAction$: Subject<void> = new Subject<void>();
  public signOutAction$: Subject<void> = new Subject<void>();

  private onDestroy$: Subject<void> = new Subject<void>();

  constructor(private authService: AuthService) {
    this.init();
  }

  private init(): void {
    this.signInAction$
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(() => this.authService.signIn());

    this.signOutAction$
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(() => this.authService.signOut());
  }

  public get isLoggedIn$(): Observable<boolean> {
    return this.authService.isLoggedIn();
  }

  public ngOnDestroy(): void {
    this.onDestroy$.next(null);
    this.onDestroy$.complete();
  }

}
