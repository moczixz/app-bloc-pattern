import { Injectable, OnDestroy } from '@angular/core';
import { WeatherService } from 'logic/services/weather.service';
import { Subject, Observable, BehaviorSubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AuthService } from 'logic/services/auth.service';

@Injectable()
export class HomeBloc implements OnDestroy {

  private weatherLocationsOutput$: BehaviorSubject<string[]> = new BehaviorSubject<string[]>([]);
  public changeLocations$: Subject<string[]> = new Subject<string[]>();

  private onDestroy$: Subject<void> = new Subject<void>();

  constructor(private weatherService: WeatherService, private authService: AuthService) {
    this.init();
  }

  private init(): void {
    this.weatherService.getCurrentLocations()
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(locations => this.weatherLocationsOutput$.next(locations));

    this.changeLocations$
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(newLocations => this.weatherService.setChangedLocations(newLocations));
  }

  public get weatherLocations$(): Observable<string[]> {
    return this.weatherLocationsOutput$.asObservable();
  }

  public get isLoggedIn$(): Observable<boolean> {
    return this.authService.isLoggedIn();
  }

  public ngOnDestroy(): void {
    this.onDestroy$.next(null);
    this.onDestroy$.complete();
  }

}
