import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { WeatherData } from 'logic/shared/interfaces/weather-data.interface';

@Injectable({
  providedIn: 'root'
})
export class WeatherApiProvider {

  constructor(private http: HttpClient) {
  }

  public getWeatherForCity(city: string): Observable<WeatherData> {
    return this.http.get<any>(`http://api.openweathermap.org/data/2.5/find?q=${city}&units=metric&appid=accde03f7b2966e40f65bb5bd7a088a9`)
      .pipe(
        map(response => response.list[0])
      );
  }

}
