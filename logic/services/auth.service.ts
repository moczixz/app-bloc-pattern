import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private isLoggedIn$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  public signIn(): void {
    // do some signIn logic
    this.isLoggedIn$.next(true);
  }

  public signOut(): void {
    // do some signOut logic
    this.isLoggedIn$.next(false);
  }

  public isLoggedIn(): Observable<boolean> {
    return this.isLoggedIn$.asObservable();
  }

}
