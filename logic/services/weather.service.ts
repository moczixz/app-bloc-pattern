import { Injectable } from '@angular/core';
import { WeatherApiProvider } from 'logic/providers/weather-api.provider';
import { interval, forkJoin, timer, Observable, BehaviorSubject, Subject } from 'rxjs';
import { startWith, mergeMap, takeUntil } from 'rxjs/operators';
import { WeatherConfig } from 'logic/shared/interfaces/weather-config.interface';
import { WeatherData } from 'logic/shared/interfaces/weather-data.interface';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  constructor(private weatherApiProvider: WeatherApiProvider) {
    console.log('weatherService constructor');
  }

  private currentLocations$: BehaviorSubject<string[]> = new BehaviorSubject<string[]>([]);
  private changedLocations$: Subject<string[]> = new Subject<string[]>();

  public shuffleLocations(locations: string[]): string[] {
    return locations
      .map((a) => ({sort: Math.random(), value: a}))
      .sort((a, b) => a.sort - b.sort)
      .map((a) => a.value);
  }

  public getWeatherForConfig(config: WeatherConfig): Observable<WeatherData[]> {
    this.currentLocations$.next(config.locations);
    return interval(config.refreshInterval)
      .pipe(
        startWith(0),
        mergeMap(() =>
          forkJoin(config.locations.slice(0, config.shuffleNumber).map(city => this.weatherApiProvider.getWeatherForCity(city)))
        ),
        takeUntil(timer(config.shuffleInterval))
      );
  }

  public getCurrentLocations(): Observable<string[]> {
    return this.currentLocations$.asObservable();
  }

  public setChangedLocations(newLocations: string[]): void {
    this.changedLocations$.next(newLocations);
  }

  public getChangedLocations(): Observable<string[]> {
    return this.changedLocations$.asObservable();
  }

}
