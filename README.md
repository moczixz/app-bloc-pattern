# AngularBlocs

Projekcik przedstawiajacy koncepcje Bloc(business logic component) pattern, stworzony pod fluttera ale pattern to pattern, mozna uzyc gdzie sie da, tu troche teori: https://medium.com/flutterpub/architecting-your-flutter-project-bd04e144a8f1


W repo, jest kilka apek w katalogu projects:
weather-brand1
weather-brand2
auth-bar -> to zrobilem jako Angular Elements a nie jako apka 


mamy tez katalog shared, w ktorym sa wspoldzielone pipesy, dyrektywy, serwisy czy co byscie chcieli.

Nasza logika jest w katalogu logic.

Jako przyklad zrobilem apke pogodowa, ta z zadania rekrutacyjnego tylko przerobilem. Mamy widget pogody (projects/weather-brand1/src/app/weather/)
jak sobie otworzycie to zobaczycie ze nie ma tam ani jednej logiki linijki, i zauwazycie pewnie ze w komponencie jest cos takiego: providers: [WeatherBloc]
to jest wrzucone w ten sposob, poniewaz od Angulara6 wszystkie serwisy sa singletonami (czy to wrzucimy do modulu czy ustawimy providedIn: root),
a my chcemy by nasz WeatherBloc nie byl jak singleton. Komponent z Blokiem komunikuje sie tylko i wylacznie poprzed rxjs, A Blok ogarnia cala logike, pyta api, walczy z serwisami itd.

odpalamy apke przez: 
ng serve --project=weather-brand1
ng serve --project=weather-brand2
ng serve --project=auth-bar

ci ktorzy nie maja zainstalowanego globalnie angular-cli, to zapraszam do package.json

Osoby ktore sa zaznajomione z reduxem, zapewne stwierdza ze mozna to samo zrobic w reduxie, owszem mozna :P ale Redux ma wiekszy prog wejscia i ma swoj swiat. Bloc Pattern wydaje mi sie ze jest duzo prostszy w ogarnieciu, i bardziej pasuje do naszego projektu.

Zalozenia sa takie: Kazdy Brand, Kazda Apka, ma swoja osobna angularowa-apke, np lvbetpl-web, lvbetpl-mobile, i inne wariacje. I kazda z nich jest obslugiwana przez Bloc'ki.
Jesli sa czesci wspolne, to mozna je wrzucac do shared ;d serwisy, komponenty, dyrektywy, pipesy, czy routingu -> wszystko.



PROBLEMY:
- co zrobic z brandami ktore roznia sie tylko kilkoma rzeczami od drugiego branda? jak np totolotek i lvbetpl terminal, totolotek ma zmienione tylko dwie rzeczy. Sa dwie mozliwosci:
1. Sklonowac caly brand lvbetpl terminal
2. Skopiowac tylko drzewko modulow angularowych, bez komponentow, same .module.ts, kazdy projekt moze uzywac komponentow z innego projektu. Jesli cos chcemy zmienic dla totolotka, to wtedy tworzymy dany komponent, i podmieniamy odpowidni import w odpowiednim module.
